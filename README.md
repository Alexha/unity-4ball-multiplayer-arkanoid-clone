# 4Ball #

[![Alt text](https://img.youtube.com/vi/L1jfRhsjQ9U/0.jpg)](https://www.youtube.com/watch?v=L1jfRhsjQ9U)

4Ball is an arkanoid/breakout clone designed for four players. It implements multiplayer with random matchmaking and no rooms, using Photon Networking. Other features include powerups, sounds and statistic tracking.

### Credits ###

* Alex Hartto - main programming and design
* Photon Unity Networking https://www.photonengine.com/en/PUN