﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTracker : MonoBehaviour {


	void OnCollisionEnter2D(Collision2D c)
	{
		if(c.collider.CompareTag("Ball"))
		{
			GetComponentInParent<PlayerStatTracker>().incrementMisses();
		}
	}
}
