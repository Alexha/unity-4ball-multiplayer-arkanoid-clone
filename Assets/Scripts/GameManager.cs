﻿using UnityEngine;
using System.Collections;

public class GameManager : Photon.PunBehaviour
{

    [SerializeField]
    GameObject _ballPrefab;

    [SerializeField]
    block[] bricks;
    float brickSize;

    [SerializeField]
    int maxDepth;

    public GameObject HUD, Gameover;
    void Awake()
    {

        DontDestroyOnLoad(gameObject);
        PhotonNetwork.automaticallySyncScene = true;
    }

    void Start()
    {
        if (PhotonNetwork.connected && !PhotonNetwork.isMasterClient)
            return;
        if(PhotonNetwork.connected && PhotonNetwork.room == null)
        {
            Debug.LogError("We are not in a room! Returning to mainmenu.");
            PhotonNetwork.LoadLevel("MainMenu");
        }
        brickSize = bricks[0].GetComponent<MeshFilter>().sharedMesh.bounds.size.x;
        if (maxDepth % 2 == 0)
            maxDepth -= 1;
        for (int i = 0; i < maxDepth; i += 2)
        {
            createRing(i + 5);
        }
        
        PhotonNetwork.Instantiate("Ball", new Vector3(0, 5f, 0), Quaternion.identity, 0);
    }

    void createBrick(Vector3 pos)
    {
        if (PhotonNetwork.connected && !PhotonNetwork.isMasterClient)
            return;
        if(Random.value < 0.9f)
            PhotonNetwork.Instantiate("Brick", pos, Quaternion.identity, 0);
        else
            PhotonNetwork.Instantiate("ReinforcedBlock", pos, Quaternion.identity, 0);
    }

    void createBrick(string brick, Vector3 pos)
    {
        if (PhotonNetwork.connected && !PhotonNetwork.isMasterClient)
            return;
        PhotonNetwork.Instantiate(brick, pos, Quaternion.identity, 0);
    }


    public void addRing()
    {
        GameObject[] blocks = GameObject.FindGameObjectsWithTag("Brick");
        foreach (var block in blocks)
        {
            // make sure it's not on outer edge
            float posX = block.transform.position.x / (brickSize);
            float posY = block.transform.position.y / (brickSize);
            float absX = Mathf.Abs(posX);
            float absY = Mathf.Abs(posY);
            if (posX < maxDepth && posY < maxDepth &&
            posX > -maxDepth && posY > -maxDepth)
            {
                // make sure not corner case
                if (absX != absY)
                {
                    if (absX > absY)
                    {

                        if (posX < 0)
                            block.transform.Translate(-brickSize, 0, 0);
                        else
                            block.transform.Translate(brickSize, 0, 0);
                    }
                    else
                    {
                        if (posY < 0)
                            block.transform.Translate(0, -brickSize, 0);
                        else
                            block.transform.Translate(0, brickSize, 0);
                    }


                }
                else
                {
                    if (posX < 0)
                        block.transform.Translate(-brickSize, 0, 0);
                    else
                        block.transform.Translate(brickSize, 0, 0);
                    if (posY < 0)
                        block.transform.Translate(0, -brickSize, 0);
                    else
                        block.transform.Translate(0, brickSize, 0);
                    // also need to fill the gaps 
                    // do the checks from bottom left, go ccw
                    if (posX < 0 && posY < 0)
                    {
                        createBrick( new Vector3(posX * brickSize - brickSize,
                           posY,
                        0));
                        createBrick( new Vector3(posX * brickSize,
                             posY - brickSize,
                             0));
                    }
                    else if (posX > 0 && posY < 0)
                    {
                        createBrick( new Vector3(posX * brickSize + brickSize,
                            posY, 0
                          ));
                        createBrick( new Vector3(posX * brickSize,

                            posY - brickSize,
                            0));
                    }
                    else if (posX > 0 && posY > 0)
                    {
                        createBrick( new Vector3(posX * brickSize + brickSize,

                           posY, 0
                           ));
                        createBrick( new Vector3(posX * brickSize,

                            posY + brickSize,
                            0));
                    }
                    // top left now
                    else
                    {
                        createBrick( new Vector3(posX * brickSize - brickSize,

                           posY,
                           0));
                        createBrick( new Vector3(posX * brickSize,

                            posY + brickSize, 0));
                    }
                }

            }
            else
            {
                Destroy(block);
            }

        }
        createRing(5);
    }
    void createRing(int depth)
    {
        float startY;
        float startX;
        startX = startY = -((depth - 1) / 2);
        //bottom and top row
        for (int i = 0; i < depth; i++)
        {
            createBrick(
                    new Vector3(startX + i * (brickSize),
                        startY,
                        0));
            createBrick(
                    new Vector3(startX + i * (brickSize),
                        startY + (depth - 1) * (brickSize),
                        0));
        }
        for (int i = 1; i < depth - 1; i++)
        {
            createBrick(
                    new Vector3(startX,
                        startY + i * (brickSize),
                        0));
            createBrick(
                    new Vector3(startX + (depth - 1) * (brickSize),
                        startY + i * (brickSize),
                        0));
        }
    }

    public void endRound()
    {
        HUD.SetActive(false);
        Gameover.SetActive(true);
        stopBalls(GameObject.FindGameObjectsWithTag("Ball"));
    }

    public void leaveGame()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("MainMenu");
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        endRound();
    }


    void stopBalls(GameObject[] balls)
    {
        foreach(var b in balls)
        {
            PhotonNetwork.Destroy(b);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
