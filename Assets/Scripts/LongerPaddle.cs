﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongerPaddle : MonoBehaviour
{
	public float timer = 5f;
	GameObject _targetPaddle;
	bool _enabled = false;

	void Update()
	{
		if (PhotonNetwork.connected
        && !PhotonNetwork.isMasterClient)
            return;

		if(_enabled)
		{
			timer -= Time.deltaTime;
			if(timer < 0)
			{
				_targetPaddle.transform.localScale = new Vector2(3f,1f);
				PhotonNetwork.Destroy(gameObject);
			}
		}
	}
    void OnCollisionEnter2D(Collision2D c)
    {
        if (PhotonNetwork.connected
        && !PhotonNetwork.isMasterClient)
            return;

        if (c.collider.CompareTag("Ball"))
        {
            _targetPaddle = c.gameObject.GetComponent<ballScript>().lastHit;
			// Sometimes the ball hasn't hit a paddle yet
			if(_targetPaddle)
			{
				_targetPaddle.transform.localScale = new Vector2(5f,1f);
				transform.position = new Vector2 (50f,50f);
				_enabled = true;
			}

        }
    }


}
