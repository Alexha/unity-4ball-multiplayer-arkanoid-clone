﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : Photon.PunBehaviour {
    public GameObject[] playerHitPercent;
    public GameObject[] playerScores;
    public GameObject[] players;

    public ScoreManager scoreManager;

    int[] scores, powerups;
    float[] hitPercents;
    void Awake()
    {
        int[] scores = new int[4];
        int[] powerups = new int[4];
        float[] hitPercents = new float[4];
        if (PhotonNetwork.connected &&
            PhotonNetwork.isMasterClient)
        {
            scores = new int[] { scoreManager.p1_score, scoreManager.p2_score, scoreManager.p3_score, scoreManager.p4_score };
            hitPercents = scoreManager.ReturnHitPercentage();
            powerups = scoreManager.ReturnPowerups();
            photonView.RPC("synchronizeScores", PhotonTargets.AllBuffered, scores, hitPercents, powerups);
        }

    }

    public void updateNames()
    {
        for(int i= 0; i < players.Length; i++)
        {
            if(i < PhotonNetwork.playerList.Length)
            {
                playerScores[i].transform.FindChild("Name").GetComponent<Text>().text = players[i].GetComponent<PhotonView>().owner.name;
            }
            else
            {
                playerScores[i].transform.FindChild("Name").GetComponent<Text>().text = "Computer";
            }
        }
     //p1Score.transform.FindChild("Name").GetComponent<Text>().text = p1.GetComponent<PhotonView>().owner.name;

    }

    public void updateScores()
    {
        for(int i= 0; i < players.Length; i++)
        {
                playerScores[i].transform.FindChild("Score").GetComponent<Text>().text = scores[i].ToString();
        }
       
    }

    public void updateHitPercent()
    {
        
        for(int i=0; i < players.Length; i++)
        {
            playerScores[i].transform.FindChild("HitPercent").GetComponent<Text>().text = (hitPercents[i]).ToString("P");
            if(hitPercents[i] > 0.8f)
            {
                playerScores[i].transform.FindChild("HitPercent").GetComponent<Text>().color = Color.green;
            }
            else
            if(hitPercents[i] < 0.4f)
            {
                playerScores[i].transform.FindChild("HitPercent").GetComponent<Text>().color = Color.red;
            }
            else
            {
                playerScores[i].transform.FindChild("HitPercent").GetComponent<Text>().color = Color.yellow;
            }

        }
    }

    public void updatePowerUps()
    {
        
        for(int i=0; i < players.Length; i++)
        {
            playerScores[i].transform.FindChild("PowerupsPicked").GetComponent<Text>().text = powerups[i].ToString();
        }
    }

    [PunRPC]
    public void synchronizeScores(int[] s, float[] hhp, int[] pu)
    {
        scores = s;
        Debug.Log(s[0] + " " + scores[0]);
        hitPercents = hhp;
        powerups = pu;

        updateNames();
        updateScores();
        updateHitPercent();
        updatePowerUps();
    }
}
