﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvedCorners : Photon.MonoBehaviour
{


    public float timeAmount = 5f;
    float _timer;
    GameObject _corners;
    bool _inPlace = false;
    void Start()
    {
        _corners = GameObject.Find("CurvedCornersObject");
        _corners.GetComponentInChildren<TextMesh>().text = System.String.Format("{0:0.##}", _timer.ToString());
        if (_corners == null)
        {
            Debug.LogError("Couldn't find CurvedCornersObject! Got: " + GameObject.Find("CurvedCornersObject"));
            DestroyPowerup();
        }
        GetComponent<MeshRenderer>().material.color = Color.cyan;
    }

    void Update()
    {
        if (_inPlace == true)
        {
            _timer -= Time.deltaTime;
            _corners.GetComponentInChildren<TextMesh>().text = System.String.Format("{0:0.}", _timer.ToString());
            if (_timer < 0)
            {
                if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
                {
                    Debug.Log("Destroying CurvedCorners by " + photonView.owner.name);
                    photonView.RPC("deactivateEffect", PhotonTargets.AllBuffered);
                    DestroyPowerup();
                }
            }
        }

    }
    void OnCollisionEnter2D(Collision2D c)
    {
        if (PhotonNetwork.connected
        && !PhotonNetwork.isMasterClient)
            return;

        if (c.collider.CompareTag("Ball"))
        {
            if (_corners.transform.position != Vector3.zero)
            {
                photonView.RPC("activateEffect", PhotonTargets.All);
            }
            else
            {
                PhotonNetwork.Destroy(gameObject);
            }

        }
    }

    [PunRPC]
    void activateEffect()
    {
        _corners.transform.position = Vector3.zero; // move the transform from elsewhere from the map to origin
        transform.position = new Vector3(50f, 50f, 0); // move the pickup somewhere else momentarily
        _timer = timeAmount;
        _inPlace = true;
    }
    [PunRPC]
    void deactivateEffect()
    {
         _corners.transform.position = new Vector3(50f, 50f, 0);
    }

    void DestroyPowerup()
    {
        if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
            PhotonNetwork.Destroy(gameObject);
    }
}
