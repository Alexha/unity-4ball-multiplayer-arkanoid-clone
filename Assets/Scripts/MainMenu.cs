﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : Photon.PunBehaviour
{
    public Text status;
    public PhotonLogLevel Loglevel = PhotonLogLevel.Informational;
    public Canvas lobby;
    public Canvas menu;
    string _version = "1.0.0";

    #region Callbacks
    // Use this for initialization
    void Awake()
    {
        PhotonNetwork.automaticallySyncScene = true;
        status.text = _version;

    }

    void Start()
    {
        if (!PhotonNetwork.connected)
        {
            status.text = "Connecting to main server";
            if (!PhotonNetwork.ConnectUsingSettings(_version))
            {
                Debug.LogWarning("Failed to connect to master server!");
                status.text = "Failed to connect to master server!";
            }
        }
    }

    public override void OnConnectedToMaster()
    {
        menu.gameObject.SetActive(true);
        status.text = "Connected to master server. Version: " + _version;
    }


    public override void OnPhotonRandomJoinFailed(object[] codeMsg)
    {

        lobby.gameObject.SetActive(false);
        menu.gameObject.SetActive(true);
        status.text = "Failed to join a random room!";
        Debug.LogWarning("Failed to join a random room, returning to mainmenu");
    }
    #endregion

    public void StartGame()
    {
        if (PhotonNetwork.connected)
        {
            CreateRoom();
        }
        else
        {
            status.text = "Not connected to master server!";
        }
    }
    void CreateRoom()
    {
        status.text = "Creating a new room";
        if (PhotonNetwork.CreateRoom("", new RoomOptions() { MaxPlayers = 4 }, null))
        {
            status.text = "Created a room. In a lobby. ";
            lobby.gameObject.SetActive(true);
            menu.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogWarning("Failed to create a room!");
            status.text = "Failed to create a room!";
            if (PhotonNetwork.room != null)
            {
                status.text += " Room already exists.";
            }
            else
            {
                status.text += " Check the logs.";
            }
        }


    }
    public override void OnJoinedRoom()
    {
        lobby.gameObject.SetActive(true);
        lobby.gameObject.transform.Find("LobbyManager").GetComponent<Lobby>().OnJoinedRoom();
    }

    public void JoinGame()
    {
        if(PhotonNetwork.JoinRandomRoom())
        {
            status.text = "Joining a random room";
            menu.gameObject.SetActive(false);
            lobby.gameObject.SetActive(true);
        }
        else
            status.text = "No rooms to join!";
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
