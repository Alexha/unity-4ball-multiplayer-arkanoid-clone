﻿using UnityEngine;
using System.Collections;

public class Goal : Photon.MonoBehaviour
{

    [SerializeField]
    private ScoreManager sm;
    [SerializeField]
    private GameManager gm;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (PhotonNetwork.connected && !PhotonNetwork.isMasterClient)
        {
            return;
        }

        if (c.gameObject.CompareTag("Ball"))
        {

            // scoring 
            GameObject lastHit = c.gameObject.GetComponent<ballScript>().lastHit;

            if (lastHit != null)
            {
                switch (lastHit.name)
                {
                    case "Player1":
                        sm.p1_score++;
                        break;
                    case "Player2":
                        sm.p2_score++;
                        break;
                    case "Player3":
                        sm.p3_score++;
                        break;
                    case "Player4":
                        sm.p4_score++;
                        break;
                    default:
                        break;
                }
                sm.UpdateScores();
            }
            // handle bricks
            gm.addRing();
            if (c.GetComponent<ballScript>().temporary)
            {
                PhotonNetwork.Destroy(c.gameObject);
                return;
            }
            
            c.transform.position = lastHit.transform.position;
            c.transform.position += lastHit.transform.up * 2f;
            c.GetComponent<ballPhysics>().setVelocity(lastHit.transform.up);
        }
    }

    void OnTriggerStay(Collider c)
    {
        if (PhotonNetwork.connected && !PhotonNetwork.isMasterClient)
        {
            return;
        }
        // clean up extra bricks
        if (c.CompareTag("Brick"))
        {
            Debug.Log("brick");
            Destroy(c.gameObject);
        }
    }
}
