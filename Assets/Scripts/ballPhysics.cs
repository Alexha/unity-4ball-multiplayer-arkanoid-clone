﻿using UnityEngine;

public class ballPhysics : Photon.PunBehaviour, IPunObservable
{
    public AudioClip ballHit, playerHit, goalHit;
    public AudioSource ballAud,powerupAud, goalAud;
    [SerializeField]
    public Vector3 velocity;
    public bool stopped;
    public Vector2 launchDir;
    Vector3 _prevVel;
    public float maxVelocity = 1f;
    [SerializeField]
    float speedChange = 0.05f;

    [SerializeField]
    float speed = 0.3f;
    Vector3 _networkPosition;
    bool audioMuted;

    #region Callbacks
    // Use this for initialization
    void Start()
    {
        var settings = FindObjectOfType<GameSettings>();
        audioMuted = settings.isMuted();
        if (PhotonNetwork.connected
        && !PhotonNetwork.isMasterClient)
            return;

        Launch();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (stopped)
            return;

        if (velocity.magnitude > maxVelocity)
            velocity = velocity.normalized * maxVelocity;
        if (velocity.z != 0)
            velocity.z = 0;
        transform.position += velocity;
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D c)
    {
        if (stopped)
            return;
        checkAudio(c.collider.tag);
        if (c.collider.CompareTag("Ball"))
        {
            return;
        }

        
        
        if (PhotonNetwork.connected &&
        !PhotonNetwork.isMasterClient)
        {
            return;
        }


        ContactPoint2D cp = c.contacts[0];
        Vector3 changedVelocity = velocity - 2f * Vector3.Project(velocity, cp.normal);
        // RED = NORMAL
        //Debug.DrawRay(cp.point, cp.normal, Color.red, 3f);
        // GREEN = _velocity
        Debug.DrawRay(cp.point, velocity, Color.green, 3f);
        // MAGENTA = CHANGED VEL
        Debug.DrawRay(cp.point, changedVelocity, Color.magenta, 3f);
        if (c.collider.CompareTag("Player"))
        {
            changedVelocity = calculatePaddleHitVelocity(c.transform, cp.point, c.collider.bounds.size.y).normalized * speed;
        }
        _prevVel = velocity;
        if (_prevVel == -velocity)
        {
            Debug.Log("Correcting velocity!");
            velocity += new Vector3(Random.value, Random.value, 0);
        }
        velocity = changedVelocity * (1 + speedChange);

        if (PhotonNetwork.isMasterClient)
            photonView.RPC("updatePUNVelocity", PhotonTargets.Others, changedVelocity, transform.position, false);
        // // FINAL _velocity = BLUE
        // Debug.DrawRay(cp.point, changedVelocity, Color.blue, 3f);


    }


    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }

    #endregion

    public void Launch()
    {
        if (PhotonNetwork.connected &&
        !PhotonNetwork.isMasterClient)
        {
            return;
        }

        velocity = new Vector3(Random.value, Random.value, 0);
        velocity = velocity.normalized * speed;

        if (PhotonNetwork.isMasterClient)
            photonView.RPC("updatePUNVelocity", PhotonTargets.Others, velocity, transform.position, false);

    }

    public void Launch(Vector2 dir)
    {
        if (PhotonNetwork.connected &&
        !PhotonNetwork.isMasterClient)
        {
            return;
        }

        velocity = dir;
        velocity = velocity.normalized * speed;

        if (PhotonNetwork.isMasterClient)
            photonView.RPC("updatePUNVelocity", PhotonTargets.Others, velocity, transform.position, false);

    }

    public void setVelocity(Vector3 dir)
    {
        if (PhotonNetwork.connected &&
        !PhotonNetwork.isMasterClient)
        {
            return;
        }

        _prevVel = velocity;
        velocity = dir;
        velocity = velocity.normalized * speed;

        if (_prevVel == -velocity)
        {
            Debug.Log("Correcting velocity!");
            velocity += new Vector3(Random.value, Random.value, 0);
        }

        if (PhotonNetwork.isMasterClient)
            photonView.RPC("updatePUNVelocity", PhotonTargets.Others, velocity, transform.position, false);


    }

    public void checkAudio(string tag)
    {
        if(audioMuted)
            return;
        if(tag == "PowerUp")
        {
            if(!powerupAud.isPlaying)
                powerupAud.PlayOneShot(playerHit);
        }
        else
        if(tag == "Goal")
        {
            if(!goalAud.isPlaying)
                goalAud.PlayOneShot(goalHit);
        }
        else
        {
                ballAud.PlayOneShot(ballHit);
                ballAud.pitch = velocity.magnitude + 0.5f;   
        }
        
    }

    public void muteAudio(bool isMuted)
    {
        audioMuted = isMuted;
    }


    Vector3 calculatePaddleHitVelocity(Transform paddle, Vector3 hitpoint, float width)
    {
        var adjVel = Vector3.Project(hitpoint - paddle.position, paddle.right) / width;
        var normalVel = Vector3.Project(velocity, paddle.up);
        if (normalVel.magnitude < 0f)
            return velocity;

        return adjVel - normalVel;
    }

    [PunRPC]
    public void updatePUNVelocity(Vector3 vel, Vector3 pos, bool stop = false)
    {
        velocity = vel;
        transform.position = pos;
        stopped = stop;
    }
    [PunRPC]
    public void updatePUNVelocity(Vector2 vel, Vector2 pos, bool stop = false)
    {
        velocity = vel;
        transform.position = pos;
        stopped = stop;
    }


    public void updateVelocities(Vector2 vel, Vector2 pos, bool stop = false)
    {
        velocity = vel;
        transform.position = pos;
        stopped = stop;
        photonView.RPC("updatePUNVelocity", PhotonTargets.AllBuffered, vel, pos, stop);
    }
}
