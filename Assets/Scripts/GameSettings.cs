﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameSettings : MonoBehaviour {


	public float roundTime=0;
	Text _timetext;

	bool _audioMuted;
	public Toggle audioToggle;
	public static string playerAudioPrefKey = "PlayerAudio";
	public void Start()
	{
		if(PlayerPrefs.HasKey(playerAudioPrefKey))
		{
			var muted = PlayerPrefs.GetString(playerAudioPrefKey);
			_audioMuted = bool.Parse(muted);
			audioToggle.isOn = _audioMuted;
		}
		else
			_audioMuted = false;
		DontDestroyOnLoad(gameObject);
	}

	public void setRoundTime(float time) 
	{
		roundTime = time;
		_timetext = GameObject.Find("TimeText").GetComponent<Text>();
		if(_timetext) 
			_timetext.text =  System.String.Format("{0:00}:00",  time);
	}

	public void muteAudio(bool isMuted)
	{
		PlayerPrefs.SetString(playerAudioPrefKey, isMuted.ToString());
		PlayerPrefs.Save();
		_audioMuted = isMuted;
		Debug.Log(audioToggle.isOn);
	}

	public bool isMuted()
	{
		return !_audioMuted;
	}
}

