﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyPaddle : Photon.MonoBehaviour
{
    public float powerupTime = 5f;

    void Start()
    {
        GetComponent<MeshRenderer>().material.color = Color.green;
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (PhotonNetwork.connected
        && !PhotonNetwork.isMasterClient)
            return;
        if (c.collider.CompareTag("Ball"))
        {
            Debug.Log("Ball hit sticky effect");
            GameObject hit = c.gameObject.GetComponent<ballScript>().lastHit;
            if (hit
                && hit.GetComponent<MouseControl>()
                && hit.GetComponent<StickyEffect>() == null)
            {
                if (PhotonNetwork.connected)
                        photonView.RPC("attachStickyEffect", PhotonTargets.AllBuffered, hit.GetPhotonView().viewID);
                    else
                        attachStickyEffect(hit.GetPhotonView().viewID);
                PhotonNetwork.Destroy(gameObject);
            }
            else
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
    [PunRPC]
    void attachStickyEffect(int view)
    {
        PhotonView.Find(view).gameObject.AddComponent<StickyEffect>();
        PhotonView.Find(view).gameObject.GetComponent<StickyEffect>().timer = powerupTime;
    }
}
