using UnityEngine;
using System.Collections;

public class block : Photon.PunBehaviour
{
    public GameObject[] powerups;
    public float powerupChance;

    public float health = 1;
    // Use this for initialization
    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);
    }
    
    void OnCollisionEnter2D(Collision2D c)
    {
        if (PhotonNetwork.connected
            && !PhotonNetwork.isMasterClient)
            return;

        if (c.collider.gameObject.CompareTag("Ball"))
        {
            if (health == 1)
            {
                if (Random.value <= powerupChance)
                {
                    SpawnPowerup();
                }
                PhotonNetwork.Destroy(gameObject);
            }
            else
            {
                health--;
                photonView.RPC("ChangeColor", PhotonTargets.AllBuffered, 0.5f,0.5f,1.0f);
            }
        }
    }
    [PunRPC]
    void ChangeColor(float r,float g, float b)
    {
        GetComponent<MeshRenderer>().material.color = new Color(r,g,b);
    }
    void SpawnPowerup()
    {
        PhotonNetwork.Instantiate
        (
            powerups[Random.Range(0, powerups.Length)].name,
            transform.position,
            Quaternion.identity,
            0
        );
    }
}
