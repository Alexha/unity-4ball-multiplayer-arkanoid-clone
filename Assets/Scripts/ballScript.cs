﻿using UnityEngine;
using System.Collections;

public class ballScript : Photon.PunBehaviour
{

    public GameObject lastHit;
    public bool temporary; // Will this ball only last until destroyed?
    public Color _serializeColor;
    public void spawnBall(Vector3 pos)
    {
        PhotonNetwork.Instantiate("Ball", pos, Quaternion.identity, 0);
    }
    // Use this for initialization

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (PhotonNetwork.connected
                && !PhotonNetwork.isMasterClient)
            return;

        if (c.collider.CompareTag("Goal"))
        {
            if (temporary)
            {
                PhotonNetwork.Destroy(gameObject);
                return;
            }
            if (lastHit != null)
            {
                transform.position = lastHit.transform.up * 2f + lastHit.transform.position;
                transform.Translate(lastHit.transform.up);
                GetComponent<ballPhysics>().Launch();
            }
            else
            {
                transform.position = new Vector3(5f, 0.5f, 0);
                GetComponent<ballPhysics>().Launch();
            }
        }

        // Save reference for scoring purposes (last player to hit the ball gets the point)
        if (c.gameObject.CompareTag("Player"))
        {
            if (PhotonNetwork.connected
                && !PhotonNetwork.isMasterClient)
                return;
            lastHit = c.gameObject;

            GetComponent<MeshRenderer>().material.color = c.gameObject.GetComponent<MeshRenderer>().material.color;
            if (PhotonNetwork.connected)
            {
                photonView.RPC("updateColors", PhotonTargets.AllBuffered,
                    GetComponent<MeshRenderer>().material.color.r,
                    GetComponent<MeshRenderer>().material.color.g,
                    GetComponent<MeshRenderer>().material.color.b);
            }

        }
        if(c.gameObject.CompareTag("PowerUp"))
        {
            if(lastHit)
                lastHit.GetComponentInParent<PlayerStatTracker>().incrementPowerups();
        }
    }

    [PunRPC]
    void updateColors(float r, float g, float b)
    {
        GetComponent<MeshRenderer>().material.color = new Color(r,g,b);
    }
}
