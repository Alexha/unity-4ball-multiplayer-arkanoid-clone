﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class PlayerNameInput : MonoBehaviour {

	static string playerNamePrefKey = "PlayerName";
	void Start () {
		if(PlayerPrefs.HasKey(playerNamePrefKey))
		{
			string name = PlayerPrefs.GetString(playerNamePrefKey);
			GetComponent<InputField>().text = name;
			PhotonNetwork.playerName = name;
		}
		else
		{
			PhotonNetwork.playerName = "Player";
		}
	}
	
	public void SetPlayerName(string name) 
	{
		PhotonNetwork.playerName = name + " ";
		PlayerPrefs.SetString(playerNamePrefKey, name);
	}
}
