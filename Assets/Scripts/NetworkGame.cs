﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkGame : Photon.PunBehaviour
{


    public GameObject[] paddles;

    #region Callbacks

    void Start()
    {
        if (PhotonNetwork.isMasterClient)
        {
            SetupPlayers();
        }
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        Debug.Log("Lost connection! Returning to main menu. " + cause);
        PhotonNetwork.LoadLevel("MainMenu");
    }

    #endregion

    void SetupPlayers()
    {
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            photonView.RPC("togglePlayerControl", PhotonTargets.AllBuffered, paddles[i].GetPhotonView().viewID);
            TransferOwnership(paddles[i], PhotonNetwork.playerList[i]);
        }

        for(int i = PhotonNetwork.playerList.Length; i < 4; i++)
        {
            photonView.RPC("toggleAIControl", PhotonTargets.AllBuffered, paddles[i].GetPhotonView().viewID);
        }
    }

    void TransferOwnership(GameObject obj, PhotonPlayer p)
    {
        if (obj.GetComponent<PhotonView>().owner != p)
        {
            obj.GetComponent<PhotonView>().TransferOwnership(p);
            Debug.Log("Transferring ownership of " + obj.name + " to " + p.name);
        }
    }

    [PunRPC]
    void togglePlayerControl(int view)
    {
        PhotonView.Find(view).GetComponent<MouseControl>().enabled = true;
        Destroy(PhotonView.Find(view).GetComponent<AI>());
    }

    [PunRPC]
    void toggleAIControl(int view)
    {
        Destroy(PhotonView.Find(view).GetComponent<MouseControl>());
    }
}
