﻿using System;
using UnityEngine;

public class FreezePaddle : Photon.MonoBehaviour
{
    public float powerupTimer = 5f;
    bool _inPlace = false;
    GameObject _player;
    Color _playerColor;
    bool _playerAI;

    void Start()
    {
        GetComponent<MeshRenderer>().material.color = Color.red;
    }
    void Update()
    {
        if (_inPlace == true)
        {
            powerupTimer -= Time.deltaTime;
            if (powerupTimer < 0)
            {
                
                Debug.Log("Stop PLayercolor: " + _playerColor.ToString());
                Debug.Log("Reenabling player :" + _player.name);
                startPlayer(_player);                
                Debug.Log("New PLayercolor: " + _playerColor.ToString());
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.collider.CompareTag("Ball"))
        {
            _player = c.collider.gameObject.GetComponent<ballScript>().lastHit;
            if (_player)
            {
                Debug.Log("Stopping player :" + _player.name);
                _inPlace = true;
                transform.position = new Vector2(50f, 50f);
                stopPlayer(_player);
            }
            else
            {
                Debug.Log("Player not found");
                if (PhotonNetwork.connected)
                {
                    if (PhotonNetwork.isMasterClient)
                    {
                        photonView.RequestOwnership();
                        PhotonNetwork.Destroy(gameObject);
                    }
                }
                else
                    PhotonNetwork.Destroy(gameObject);
            };
        }
    }

    private void stopPlayer(GameObject player)
    {
        _playerColor = player.GetComponent<MeshRenderer>().material.color;
        Debug.Log("PLayercolor: " + _playerColor.ToString());
        if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
            photonView.RPC("changePaddleColor", PhotonTargets.AllBuffered, player.GetPhotonView().viewID, 1f, 0f, 0f, _playerColor.r, _playerColor.g, _playerColor.b);
        else if (!PhotonNetwork.connected)
            player.GetComponent<MeshRenderer>().material.color = Color.red;
        Debug.Log("New PLayercolor: " + _playerColor.ToString());
        if (player.GetComponent<AI>())
        {
            _playerAI = true;
            Debug.Log("Player " + _player.name + " is an AI");
            player.GetComponent<AI>().enabled = false;
            return;
        }
        else
        if (player.GetComponent<MouseControl>())
        {
            Debug.Log("Player " + _player.name + " is not an AI");
            if(PhotonNetwork.connected && PhotonNetwork.isMasterClient)
            {
                photonView.RPC("togglePlayer", PhotonTargets.OthersBuffered, player.GetPhotonView().viewID, false);
            }
            _playerAI = false;
            player.GetComponent<MouseControl>().enabled = false;
            return;
        }
        else
        if (!player.GetComponent<MouseControl>()
            && !player.GetComponent<AI>())
        {
            Debug.Log("Player already stopped.");
            return;
        }
        else
            Debug.LogError("Error, player movement scripts are not enabled or are missing!");
    }

    private void startPlayer(GameObject player)
    {
        if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
            photonView.RPC("changePaddleColor", PhotonTargets.AllBuffered, player.GetPhotonView().viewID, _playerColor.r, _playerColor.g, _playerColor.b, 0f, 0f, 0f);
        else
            player.GetComponent<MeshRenderer>().material.color = _playerColor;
        Debug.Log("Restarting player");
        if (_playerAI)
            player.GetComponent<AI>().enabled = true;
        else
        {
            player.GetComponent<MouseControl>().enabled = true;
            if(PhotonNetwork.connected && PhotonNetwork.isMasterClient)
            {
                photonView.RPC("togglePlayer", PhotonTargets.OthersBuffered, player.GetPhotonView().viewID, false);
            }
        }
    }

    [PunRPC]
    void changePaddleColor(int viewID, float r, float g, float b, float or, float og, float ob)
    {
        PhotonView.Find(viewID).GetComponent<MeshRenderer>().material.color = new Color(r, g, b);
        _playerColor = new Color(or,og, ob);
    }

    [PunRPC]
    void togglePlayer(int viewID, bool isOn)
    {
        var control = PhotonView.Find(viewID).GetComponent<MouseControl>();
        if(control)
            control.enabled = isOn;
    }
}
