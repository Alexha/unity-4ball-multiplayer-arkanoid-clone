﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitBall : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D c)
    {
        if (PhotonNetwork.connected
        && !PhotonNetwork.isMasterClient)
            return;

        if (c.collider.CompareTag("Ball"))
        {
            var balls = GameObject.FindGameObjectsWithTag("Ball");
            // Only spawn ten balls
            if (balls.Length > 10)
            {
                PhotonNetwork.Destroy(gameObject);
                return;
            }

            GameObject newball = PhotonNetwork.Instantiate("Ball", c.transform.position, Quaternion.identity, 0);
            Debug.Log("Velocity before split: " + c.gameObject.GetComponent<ballPhysics>().velocity);
            c.gameObject.GetComponent<ballPhysics>().setVelocity(Quaternion.Euler(0, 0, 45) * c.gameObject.GetComponent<ballPhysics>().velocity);
            Debug.Log("Velocity after split: " + c.gameObject.GetComponent<ballPhysics>().velocity);
            newball.GetComponent<ballPhysics>().setVelocity(Quaternion.Euler(0, 0, -45) * newball.GetComponent<ballPhysics>().velocity);
            newball.GetComponent<ballScript>().temporary = true; // These balls only last until they leave the field
            PhotonNetwork.Destroy(gameObject);
        }
    }
}
