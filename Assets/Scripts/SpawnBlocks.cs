﻿    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SpawnBlocks : MonoBehaviour
    {
        public float powerupTime = 5f;
        public GameObject[] spawnAreas;
        public float brickSz;
        void Start()
        {
            spawnAreas = GameObject.FindGameObjectsWithTag("BlockSpawns");
            GetComponent<MeshRenderer>().material.color = Color.black;
        }

        void Update()
        {
            if (PhotonNetwork.connected
            && !PhotonNetwork.isMasterClient)
                return;
            powerupTime -= Time.deltaTime;
            if (powerupTime < 0)
                PhotonNetwork.Destroy(gameObject);
        }

        void OnCollisionEnter2D(Collision2D c)
        {
            if (PhotonNetwork.connected
            && !PhotonNetwork.isMasterClient)
                return;

            if (c.collider.CompareTag("Ball"))
            {
                GameObject hit = c.gameObject.GetComponent<ballScript>().lastHit;
                if (hit)
                {
                    foreach (var player in spawnAreas)
                    {
                        if (player != hit)
                        {
                            Spawn(player);
                        }
                    }
                }
                PhotonNetwork.Destroy(gameObject);
            }
        }

        void Spawn(GameObject area)
        {
            if (PhotonNetwork.connected
                && !PhotonNetwork.isMasterClient)
                return;
            var randomPos = GenerateRandomPos(area.GetComponent<BoxCollider2D>().bounds);
            if(!CheckIfBricks(Physics.OverlapSphere(randomPos, brickSz)))
                PhotonNetwork.Instantiate("ReinforcedBlock", randomPos, Quaternion.identity, 0); 
            else
                Spawn(area);
        }

        Vector2 GenerateRandomPos(Bounds bounds)
        {
            return new Vector2
            (
                Random.Range((int)(bounds.center.x - bounds.extents.x), (int)(bounds.center.x + bounds.extents.x)),
                Random.Range((int)(bounds.center.y - bounds.extents.y),(int) (bounds.center.x + bounds.extents.y))
            );
        }

        bool CheckIfBricks(Collider[] Collisions)
        {
            if (Collisions.Length != 0)
            {
                foreach (var c in Collisions)
                { 
                    if(c.gameObject.tag == "Brick" || c.gameObject.tag == "PowerUp")
                        return true;
                }
            }
            // no bricks found
            return false;
        }
    }
