﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    public GameObject hudCanvas;
    private bool _paused = false;

    public void LeaveButton() 
	{
		PhotonNetwork.LeaveRoom();
		PhotonNetwork.LoadLevel("MainMenu");
	}

	public void OptionsButton() 
	{
		return;
	}
}
