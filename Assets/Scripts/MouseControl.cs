﻿using UnityEngine;
using System.Collections;

public class MouseControl : Photon.PunBehaviour
{

    private float distanceFromCamera;
    private float boundX;
    private Vector3 actualPosition;
    public MeshRenderer rightwall, leftwall;


    void Start()
    {
        distanceFromCamera = Vector3.Distance(gameObject.transform.position, Camera.main.transform.position);

    }

    void Update()
    {
        if (PhotonNetwork.connected
            && !photonView.isMine)
            return;
        
        Vector3 camCoords = Camera.main.ScreenToWorldPoint(getMouseCoords());
        Vector3 result = Vector3.Dot(transform.right, camCoords) * transform.right;

        Move(result);
    }

    public void Move(Vector3 worldpos)
    {
        Vector3 prev = transform.localPosition;
        // Debug.DrawLine(Vector3.zero, worldpos, Color.red);
        //Debug.Log("worldpos: " + worldpos);
        worldpos = transform.worldToLocalMatrix.MultiplyVector(worldpos);
        worldpos = transform.localScale.x * worldpos;
        //Debug.Log(transform.worldToLocalMatrix);
        // Debug.Log("Local: " + worldpos);

        transform.localPosition = new Vector3(worldpos.x,
                                         transform.localPosition.y,
                                         transform.localPosition.z);

        if (gameObject.GetComponent<MeshRenderer>().bounds.Intersects(leftwall.bounds) ||
            gameObject.GetComponent<MeshRenderer>().bounds.Intersects(rightwall.bounds))
        {
            transform.localPosition = prev;
        }
    }

    Vector3 getMouseCoords()
    {
        Vector3 m_coord = Input.mousePosition;
        m_coord.z = distanceFromCamera;
        return m_coord;
    }
}
