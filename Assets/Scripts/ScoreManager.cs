﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ScoreManager : Photon.PunBehaviour
{
    public int p1_score, p2_score, p3_score, p4_score = 0;

    public PlayerStatTracker[] playerTrackers;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		if(PhotonNetwork.connected)
		{
				photonView.RPC("UpdatePUNScores", PhotonTargets.AllBuffered, 0, 0, 0, 0);
		}
		else
		{
			gameObject.GetComponent<Text>().text = p1_score + "-" +
                                                   p2_score + "-" +
                                                   p3_score + "-" +
                                                   p4_score;
		}
	}

    public int[] ReturnScores()
    {
        return new []{p1_score, p2_score, p3_score, p4_score};
    }

    public float[] ReturnHitPercentage()
    {
        float[] hitPercents = new float[4];
        for (int i = 0; i < 4; i++)
        {
            hitPercents[i] = playerTrackers[i].returnHitPercentage();
        }
        return hitPercents;
    }

    public int[] ReturnPowerups()
    {
        int[] powerups = new int[4];
        for (int i =0; i < 4; i++)
        {
            powerups[i] = playerTrackers[i].powerUpsPicked();
        }
        return powerups;
    }
    public void UpdateScores()
    {
        if (PhotonNetwork.connected && !PhotonNetwork.isMasterClient)
        {
            return;
        }
        else if (PhotonNetwork.connected)
        {
            photonView.RPC("UpdatePUNScores", PhotonTargets.AllBuffered, p1_score, p2_score, p3_score, p4_score	);
        }
        else
        {

            gameObject.GetComponent<Text>().text = p1_score + "-" +
                                                   p2_score + "-" +
                                                   p3_score + "-" +
                                                   p4_score;
        }
    }

    [PunRPC]
    void UpdatePUNScores(int p1, int p2, int p3, int p4)
    {
        p1_score = p1;
        p2_score = p2;
        p3_score = p3;
        p4_score = p4;
        gameObject.GetComponent<Text>().text = p1_score + "-" +
                                               p2_score + "-" +
                                               p3_score + "-" +
                                               p4_score;
    }

}
