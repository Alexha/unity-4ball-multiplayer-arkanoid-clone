﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : Photon.PunBehaviour
{

    public Text status;

    public GameObject[] players;

    public GameObject menu;
    public GameObject lobby;

    public GameObject start, ready;
    int _playerCount = 1;
    #region Callbacks
    // Use this for initialization
    public void Awake()
    {
        Debug.Log("Created a room.");
        status.text = "In a lobby.";
        players[0].GetComponentInChildren<Text>().text = PhotonNetwork.player.name;
        players[0].GetComponentInChildren<Toggle>().isOn = false; // AI toggle
        Debug.Log(PhotonNetwork.player.name);

    }

    public override void OnJoinedRoom()
    {

        status.text = "In a lobby.";
        _playerCount = PhotonNetwork.playerList.Length;
        for (int i = 0; i < _playerCount; i++)
        {
            players[i].GetComponentInChildren<Text>().text = PhotonNetwork.playerList[i].name;
            players[i].GetComponentInChildren<Toggle>().isOn = false; // AI toggle
        }

        if(PhotonNetwork.connected)
        {
               start.SetActive(PhotonNetwork.isMasterClient);
               ready.SetActive(!PhotonNetwork.isMasterClient);
        } 
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerConnected");
        status.text = player.name + " connected!";

            _playerCount++;
            players[_playerCount - 1].GetComponentInChildren<Text>().text = player.name;
            players[_playerCount - 1].GetComponentInChildren<Toggle>().isOn = false; // AI toggle
        

    }
    public override void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Debug.Log("OnPhotonPlayerDisconnected");
        status.text = player.name + " disconnected!";

            players[_playerCount - 1].GetComponentInChildren<Text>().text = "Computer";
            players[_playerCount - 1].GetComponentInChildren<Toggle>().isOn = true; // AI toggle
            _playerCount++;
        if(PhotonNetwork.isMasterClient)
        {
            ready.SetActive(false); // Swap buttons
            start.SetActive(true);
        }
    }

    #endregion


    public void ReturnButton()
    {
        ready.SetActive(false);
        start.SetActive(true);
        menu.SetActive(true);
        lobby.SetActive(false);
        foreach (var p in players)
        {
            p.GetComponentInChildren<Text>().text = "Computer";
            p.GetComponentInChildren<Toggle>().isOn = true; // AI toggle
        }
		
		if(PhotonNetwork.room != null)
        if (!PhotonNetwork.LeaveRoom())
        {
            status.text = "Failed to leave the room!";
        }
    }

    public void StartButton()
    {
        PhotonNetwork.room.open = false; // no joining games in progress
        PhotonNetwork.LoadLevel("Game");
    }

    
    public void ReadyButton()
    {
        Debug.Log("Ready pressed");
        foreach (var p in players)
        {
            if (p.GetComponentInChildren<Text>().text == PhotonNetwork.player.name)
            {
                Debug.Log("Player found");
                var toggle = p.GetComponentInChildren<Toggle>();
                if (toggle)
                {
                    toggle.isOn = !toggle.isOn;
                    Debug.Log("Switching toggle");
                    if (PhotonNetwork.connected)
                       {
                            photonView.RPC("updateToggles", PhotonTargets.OthersBuffered, PhotonNetwork.player.name, toggle.isOn);
                       }
                }
            }
        }
    }

    [PunRPC]
    public void updateToggles(string player, bool state)
    {
        Debug.Log("player received" + player);
        foreach(var p in players)
        {
            if (p.GetComponentInChildren<Text>().text == player)
                p.GetComponentInChildren<Toggle>().isOn = state;
        }
    }
}
