﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatTracker : MonoBehaviour {

	public GameObject paddle;
	public GameObject goalArea;

	int _hitBall = 0;
	int _hitGoal = 0;
	int _powerups;


	public void incrementHits()
	{
		_hitBall++;
	}

	public void incrementMisses()
	{
		_hitGoal++;
	}
	public void incrementPowerups()
	{
		_powerups++;
	}
	public float returnHitPercentage()
	{
		return (float) _hitBall / (float)(_hitBall+_hitGoal);
	}

	public int powerUpsPicked()
	{
		return _powerups;
	}
}
