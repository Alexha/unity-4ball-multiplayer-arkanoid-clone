﻿using UnityEngine;

public class AI : Photon.PunBehaviour
{
    public float velocity = 2f;

    public MeshRenderer leftwall, rightwall;

    GameObject[] _targets;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.connected &&
        !PhotonNetwork.isMasterClient)
        {
            return;
        }
            _targets = GameObject.FindGameObjectsWithTag("Ball");
            if (_targets.Length > 0)
                MoveTowards(_targets[0]);
    }

    void MoveTowards(GameObject target)
    {
        if (target)
        {
            Vector3 prev = transform.localPosition;
            Vector3 projection = Vector3.ProjectOnPlane(target.transform.position, transform.up);
            Move(projection);
        }
    }

        public void Move(Vector3 worldpos)
    {
        Vector3 prev = transform.localPosition;
        // Debug.DrawLine(Vector3.zero, worldpos, Color.red);
        //Debug.Log("worldpos: " + worldpos);
        worldpos = transform.worldToLocalMatrix.MultiplyVector(worldpos);
        worldpos = transform.localScale.x * worldpos;
        //Debug.Log(transform.worldToLocalMatrix);
        // Debug.Log("Local: " + worldpos);

        transform.localPosition = new Vector3(worldpos.x,
                                         transform.localPosition.y,
                                         transform.localPosition.z);

        if (gameObject.GetComponent<MeshRenderer>().bounds.Intersects(leftwall.bounds) ||
            gameObject.GetComponent<MeshRenderer>().bounds.Intersects(rightwall.bounds))
        {
            transform.localPosition = prev;
        }
    }

}
