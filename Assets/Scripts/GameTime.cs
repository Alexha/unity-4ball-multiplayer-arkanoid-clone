﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTime : Photon.MonoBehaviour, IPunObservable
{
    public GameObject gameOver;
    struct RoundTime
    {
        public int minutes;
        public float seconds;
    }

    RoundTime _roundTime;

    // Use this for initialization
    void Start()
    {
        float t = FindObjectOfType<GameSettings>().roundTime;
        _roundTime.minutes = (int)t;
        _roundTime.seconds = 0;
    }

    void Update()
    {
        if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
        {
            _roundTime.seconds -= Time.deltaTime;
            if (_roundTime.seconds < 0)
            {
                _roundTime.minutes--;
                _roundTime.seconds = 59;
            }
            if (_roundTime.minutes < 0)
            {
                if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
                {
                    photonView.RPC("endGameForAll", PhotonTargets.AllBuffered);
                }
            }
        }
        GetComponent<Text>().text = System.String.Format("{0:00}:{1:00}", _roundTime.minutes, _roundTime.seconds);

    }
    [PunRPC]
    void endGameForAll()
    {
        GameObject.FindObjectOfType<GameManager>().endRound();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(_roundTime.seconds);
            stream.SendNext(_roundTime.minutes);
        }
        else
        {
            _roundTime.seconds = (float)stream.ReceiveNext();
            _roundTime.minutes = (int)stream.ReceiveNext();
        }
    }
}
