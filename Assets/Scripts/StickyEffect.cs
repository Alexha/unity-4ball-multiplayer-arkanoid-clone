﻿using System.Collections.Generic;
using UnityEngine;

public class StickyEffect : Photon.MonoBehaviour
{
    public float timer = 5f;
    List<GameObject> caughtBalls;

    void Start()
    {
        timer = 5f;
        caughtBalls = new List<GameObject>();
        Debug.Log("Sticky effect created");
    }

    void Update()
    {
        if (PhotonNetwork.connected
            && !photonView.isMine)
            return;
        if(Input.GetMouseButtonUp(0))
        {
            Debug.Log("Launching from sticky");
            LaunchBall();

        }

        timer -= Time.deltaTime;
        if (timer < 0)
        {
            Debug.Log("Time ran out, destroying sticky effect");
            LaunchBalls();
            Destroy(this);
        }
    }


    void OnCollisionEnter2D(Collision2D c)
    {
        Debug.Log("Entered sticky collision");
        if (PhotonNetwork.connected
            && !photonView.isMine)
            return;

        if (c.collider.CompareTag("Ball"))
        {
            Debug.Log("Caught ball, setting vel to zero");
            var ball = c.collider.gameObject;
            CatchBall(ball, c.collider.transform.position);
            caughtBalls.Add(ball);
        }
    }



    void CatchBall(GameObject ball, Vector2 pos)
    {
            // ball.transform.SetParent(transform);
            if(PhotonNetwork.connected) 
                photonView.RPC("parentBall",PhotonTargets.AllBuffered, ball.GetPhotonView().viewID);
            else
                parentBall(ball.GetPhotonView().viewID);
            ball.GetComponent<ballPhysics>().setVelocity(Vector3.zero);
            ball.GetComponent<ballPhysics>().stopped = true;
            ball.transform.position = pos;
    }


    void LaunchBalls()
    {
        while (caughtBalls.Count != 0)
        {
            Debug.Log("Launchingballs: " + caughtBalls[0].transform.position);
            if(caughtBalls[0] == null)
            {
                Debug.Log("No balls to launch!");
                break;
            }
            caughtBalls[0].GetComponent<ballPhysics>().setVelocity(transform.up);
            caughtBalls[0].GetComponent<ballPhysics>().stopped = false;
            if(PhotonNetwork.connected)
                photonView.RPC("unparentBall",PhotonTargets.AllBuffered, caughtBalls[0].GetPhotonView().viewID);
            else
                unparentBall(caughtBalls[0].GetPhotonView().viewID);
            caughtBalls.RemoveAt(0);
        }
    }


    void LaunchBall()
    {
        Debug.Log("Launchball called");
        if (caughtBalls.Count == 0)
            return;
        var ball = caughtBalls[0];
        if(PhotonNetwork.connected)
                photonView.RPC("unparentBall",PhotonTargets.AllBuffered, caughtBalls[0].GetPhotonView().viewID);
        else
                unparentBall(caughtBalls[0].GetPhotonView().viewID);
        ball.GetComponent<ballPhysics>().setVelocity(transform.up);
        ball.GetComponent<ballPhysics>().stopped = false;
        caughtBalls.RemoveAt(0);
        
    }

    [PunRPC]
    void parentBall(int view)
    {
        PhotonView.Find(view).transform.parent = gameObject.transform;
    }
    [PunRPC]
    void unparentBall(int view)
    {
        PhotonView.Find(view).transform.parent = null;
    }
}
